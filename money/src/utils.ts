/**
 * get a number digit for a given position
 * @param number
 * @param position
 * @returns
 */

export const getDigit = (number: number, position: number): number => {
  //return Math.floor(number / Math.pow(10, position)) % 10;
  return parseInt(number.toString()[position - 1]);
};

/**
 * Get number size in digits
 * @param value
 * @returns
 */
export const getNumberSize = (value: number): number => {
  return value.toString().length;
};

/**
 * Extract the decimal part of number
 * @param value
 * @returns
 */
export const extractCents = (value: number): number => {
  return Math.round((value % 1) * 100);
};

export const extractDollars = (value: number): number => {
  return Math.floor(value);
};

/**
 * Extract the remaining value to be processed.
 * @param value
 * @returns
 */
export const extractRemainingAmount = (value: number): number => {
  return value % Math.pow(10, getNumberSize(value) - 1);
};

/**
 * Business role that says if we should ouput dollars value
 * @param dollarsValue
 * @param centsValue
 * @returns
 */

export const shouldDisplayDollars = (
  dollarsValue: number,
  centsValue: number,
): boolean => {
  return dollarsValue > 0 || centsValue <= 0;
};

/**
 * Business role that says if we should output cents value
 * @param centsValue
 * @returns
 */
export const shouldDisplayCents = (centsValue: number): boolean => {
  return centsValue > 0;
};

/**
 * Gets the correct dollar wording (plural or singular)
 * @param dollarValue
 * @returns
 */
export const getDollarWording = (dollarValue: number): string => {
  return dollarValue > 1 || dollarValue === 0 ? 'dollars' : 'dollar';
};

/**
 * Gets the correct cents wording singular
 * @param centsValue
 * @returns
 */
export const getCentsWording = (centsValue: number): string => {
  return centsValue > 1 ? 'cents' : 'cent';
};
