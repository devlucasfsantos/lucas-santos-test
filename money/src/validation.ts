const ERRORS = {
  INVALID_CENTS: 'Invalid Cents. We only allow cents between 0 and 99',
  INVALID_VALUE:
    'Wow. You have too much money. We only deal with values between 0 and 1000',
};

export const assertCentsIsValid = (value: number): void => {
  if (value.toString().length > 2 || value > 99 || value < 0) {
    console.error(value);
    throw new Error(ERRORS.INVALID_CENTS);
  }
};

export const assertDollarIsValid = (value: number): void => {
  if (value > 1000) {
    console.error(value);
    throw new Error(ERRORS.INVALID_VALUE);
  }
};
