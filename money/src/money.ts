import {
  getDigit,
  getNumberSize,
  extractCents,
  extractDollars,
  extractRemainingAmount,
  getCentsWording,
  getDollarWording,
  shouldDisplayCents,
  shouldDisplayDollars,
} from './utils';
import {assertCentsIsValid, assertDollarIsValid} from './validation';

//ones base value  0 - 9
const ones = {
  0: 'zero',
  1: 'one',
  2: 'two',
  3: 'three',
  4: 'four',
  5: 'five',
  6: 'six',
  7: 'seven',
  8: 'eight',
  9: 'nine',
};

// tens special cases - 10-19
const tens = {
  10: 'ten',
  11: 'eleven',
  12: 'twelve',
  13: 'thirtheen',
  14: 'fourteen',
  15: 'fifteen',
  16: 'sixteen',
  17: 'seventeen',
  18: 'eighteen',
  19: 'nineteen',
};

//dozens
const dozens = {
  2: 'twenty',
  3: 'thirty',
  4: 'fourty',
  5: 'fifty',
  6: 'sixty',
  7: 'seventy',
  8: 'eighty',
  9: 'ninety',
};

export const handleOneValue = (value: number): string => {
  return ones[value];
};

/**
 * Handle dozen values. It has a special business role to check if it maches a special case of tens (10-19)
 * @param value
 * @returns
 */
export const handleDozenValue = (value: number): string => {
  const digit = getDigit(value, 1);

  if (tens[value]) {
    return tens[value];
  }

  let result = `${dozens[digit]}`;

  const resolvedRemaining = handleRemainingAmount(value);

  if (resolvedRemaining) {
    result += ` ${resolvedRemaining}`;
  }
  return result;
};

/**
 * Handle hundred values
 * * @todo the behaviour is quite similar of handle thousand but let's not abstract it as it's not necessary at this point
 * @param value
 * @returns
 */
export const handleHundredValue = (value: number): string => {
  const digit = getDigit(value, 1);
  let result = `${ones[digit]} hundred`;

  const resolvedRemaining = handleRemainingAmount(value);

  if (resolvedRemaining) {
    result += ` and ${resolvedRemaining}`;
  }
  return result;
};

/**
 * Handle thousand values.
 * @todo the behaviour is quite similar of handle hundred but let's not abstract it as it's not necessary at this point
 * @param value
 * @returns
 */

export const handleThousandValue = (value: number): string => {
  const digit = getDigit(value, 1);
  let result = `${ones[digit]} thousand`;

  const leftAmount = handleRemainingAmount(value);

  if (leftAmount) {
    result += ` and ${leftAmount}`;
  }
  return result;
};

export const handleRemainingAmount = (value: number): string | null => {
  const remaining = extractRemainingAmount(value);

  if (remaining > 0) {
    return translateNumber(remaining);
  }

  return null;
};

export const translateNumber = (value: number): string => {
  const size = getNumberSize(value);
  switch (size) {
    case 1:
      return handleOneValue(value);
    case 2:
      return handleDozenValue(value);
    case 3:
      return handleHundredValue(value);
    case 4:
      return handleThousandValue(value);
    default:
      throw new Error(`Sorry, we could not process the value ${value}`);
  }
};

const translate = (dollars: number, cents: number): string => {
  let result = null;

  const displayCents = shouldDisplayCents(cents);
  const displayDollars = shouldDisplayDollars(dollars, cents);

  const centsText = getCentsWording(cents);
  const dolarText = getDollarWording(dollars);

  const centsTranslated = translateNumber(cents);
  const dollarsTranslated = translateNumber(dollars);

  if (displayDollars) {
    result = `${dollarsTranslated} ${dolarText}`;
    result += displayCents ? ` and ${centsTranslated} ${centsText}` : '';
  } else if (displayCents) {
    result = `${centsTranslated} ${centsText}`;
  } else {
    throw new Error(
      'Oops. We did not find any valid cents/dollar value to be processed ',
    );
  }

  return result;
};

/**
 *
 * @param n
 * @returns
 */
const readNumber = (n: number): string => {
  const centsValue = extractCents(n);
  assertCentsIsValid(centsValue);
  const dollarsValue = extractDollars(n);
  assertDollarIsValid(dollarsValue);
  return translate(dollarsValue, centsValue);
};

export default readNumber;
