import * as faker from 'faker';
import {
  extractCents,
  extractDollars,
  extractRemainingAmount,
  getDigit,
} from '../src/utils';

describe('utils spect', () => {
  it('getDigit should get the N first digits of a given number', () => {
    const number = faker.random.number({
      min: 1,
      max: 4,
    });
    const numberStr = number.toString();
    const numberLength = numberStr.length;

    for (let i = 0; i < numberLength; i++) {
      expect(getDigit(number, i + 1).toString()).toEqual(numberStr[i]);
    }
  });

  it('extractCents should extract cents', () => {
    const n0 = 0;
    const n1 = 0.09;
    const n2 = 0.99;

    expect(extractCents(n0)).toBe(0);
    expect(extractCents(n1)).toBe(9);
    expect(extractCents(n2)).toBe(99);
  });

  it('extractDollars should dollars', () => {
    const n0 = 0;
    const n1 = 10.09;
    const n2 = 100.99;
    const n3 = 1000.99;

    expect(extractDollars(n0)).toBe(0);
    expect(extractDollars(n1)).toBe(10);
    expect(extractDollars(n2)).toBe(100);
    expect(extractDollars(n3)).toBe(1000);
  });

  it('extractRemainingAmount should extract the remaining value to be processed', () => {
    const n1 = 10;
    const n2 = 100;
    const n3 = 105;
    const n4 = 55;
    const n5 = 155;
    expect(extractRemainingAmount(n1)).toBe(0);
    expect(extractRemainingAmount(n2)).toBe(0);
    expect(extractRemainingAmount(n3)).toBe(5);
    expect(extractRemainingAmount(n4)).toBe(5);
    expect(extractRemainingAmount(n5)).toBe(55);
  });
});
