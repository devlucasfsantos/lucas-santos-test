import * as faker from 'faker';
import * as moneyModule from '../src/money';

describe('Application Test', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should handleRemainingAmount translate any remaining value ', () => {
    const mockTranslateNumber = jest.spyOn(moneyModule, 'translateNumber');

    const remainingValue = 95;
    const nonRemaningValue = 90;

    const nonRemainingResult = moneyModule.handleRemainingAmount(
      nonRemaningValue,
    );
    expect(nonRemainingResult).toBe(null);
    const remainingResult = moneyModule.handleRemainingAmount(remainingValue);
    expect(remainingResult).not.toBe(null);
    expect(mockTranslateNumber).toHaveBeenCalledTimes(1);
  });

  it('should translate numbers according to its category (dozens,hundres,etc) ', () => {
    const mockOneHandler = jest.spyOn(moneyModule, 'handleOneValue');
    const mockDozenHandler = jest.spyOn(moneyModule, 'handleDozenValue');
    const mockHundredHandler = jest.spyOn(moneyModule, 'handleHundredValue');
    const mockThousandValues = jest.spyOn(moneyModule, 'handleThousandValue');

    const one = faker.random.number({min: 0, max: 9});
    const dozen = faker.random.number({min: 10, max: 99});
    const hundred = faker.random.number({min: 100, max: 999});
    const thousand = faker.random.number({min: 1000, max: 9999});

    const invalidRange = faker.random.number({min: 10000, max: 9999999});
    moneyModule.translateNumber(one);
    expect(mockOneHandler).toHaveBeenCalledWith(one);
    moneyModule.translateNumber(dozen);
    expect(mockDozenHandler).toHaveBeenCalledWith(dozen);
    moneyModule.translateNumber(hundred);
    expect(mockHundredHandler).toHaveBeenCalledWith(hundred);
    moneyModule.translateNumber(thousand);
    expect(mockThousandValues).toHaveBeenCalledWith(thousand);

    expect(() => {
      moneyModule.translateNumber(invalidRange);
    }).toThrow();
  });
});
