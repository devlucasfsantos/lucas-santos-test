import readNumber from '../src/money';
import {translationCases} from './test-utils';

describe('Application End to End Test', () => {
  it('should readNumbers and output the correct value', () => {
    translationCases.forEach(x => {
      expect(readNumber(x.input)).toBe(x.output);
    });
  });
});
