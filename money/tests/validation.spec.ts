import {assertCentsIsValid, assertDollarIsValid} from '../src/validation';
import * as faker from 'faker';

describe('validation spec', () => {
  it('should allow only valid cents', () => {
    const invalid = 100;
    const oneN = 9;
    const twoN = 99;

    expect(() => {
      assertCentsIsValid(invalid);
    }).toThrow();

    expect(() => {
      assertCentsIsValid(oneN);
      assertCentsIsValid(twoN);
    }).not.toThrow();
  });

  it('should allow only valid cents', () => {
    const valid = faker.random.number({
      min: 0,
      max: 99,
    });

    const invalid = faker.random.number({
      min: 100,
      max: 1000000,
    });

    expect(() => {
      assertCentsIsValid(invalid);
    }).toThrow();

    expect(() => {
      assertCentsIsValid(valid);
    }).not.toThrow();
  });

  it('should allow only valid cents', () => {
    const valid = faker.random.number({
      min: 0,
      max: 1000,
    });

    const invalid = faker.random.number({
      min: 1001,
      max: 1000000,
    });
    expect(() => {
      assertDollarIsValid(invalid);
    }).toThrow();

    expect(() => {
      assertDollarIsValid(valid);
    }).not.toThrow();
  });
});
