interface TranslationTestCases {
  input: number;
  output: string;
}

export const translationCases: Array<TranslationTestCases> = [
  {
    input: 0,
    output: 'zero dollars',
  },
  {
    input: 1,
    output: 'one dollar',
  },
  {
    input: 10,
    output: 'ten dollars',
  },
  {
    input: 120,
    output: 'one hundred and twenty dollars',
  },
  {
    input: 1000,
    output: 'one thousand dollars',
  },
  {
    input: 0.12,
    output: 'twelve cents',
  },
  {
    input: 8.12,
    output: 'eight dollars and twelve cents',
  },
  {
    input: 80.12,
    output: 'eighty dollars and twelve cents',
  },
  {
    input: 80.75,
    output: 'eighty dollars and seventy five cents',
  },
  {
    input: 180.12,
    output: 'one hundred and eighty dollars and twelve cents',
  },
];
