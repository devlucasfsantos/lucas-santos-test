# Laybuy Coin Test

Author: Lucas Santos (lucashfreitas1@gmail.com)

## Instructions

```
# Laybuy Code Sample Test


## Instructions

Below is a description of a function to implement. It's not a trick question but there are a few
logic details to work out. We're interested in:

* How you solve a problem
* Clean readable code
* Good understanding of unit testing

There isn't any time limit but it shouldn't take more than a couple of hours, and if you are
really stuck please get in touch for a hint. We work in a team in the real world so sensible
questions wont be considered a failure!

The easiest way to submit is by writing in .NET or node.js, and sharing a github repository with
us. If you have other tools you'd like to use just ask and we'll work something out.


## Specification

Write a function which returns a dollar value written out in English words.

The function should handle all values from 0 to 1000, up to two decimal places. If there is any
ambiguity in the spec you are welcome to make a decision on an appropriate output and document it.

Include related unit tests to show that it works.

A few examples:

| Input | Output                             |
| ----- | ---------------------------------- |
| 0     | "zero dollars"                     |
| 0.12  | "twelve cents"                     |
| 10.55 | "ten dollars and fifty five cents" |
| 120   | "one hundred and twenty dollars"   |

PS: As a small hint you might find the "modulo" operator (a `%` sign in C# or JavaScript) useful to break down the value.

```

## Running the solution

- run `npm i or npm install` to install the packages
- run `npm test or npm t` to execute the tests
- run `npm start` to execute the main applications

## Solution Overview

- We first extract `cents` and `dollar` values and also check if they are valid
  inputs.
- We translate `cents` and `dolars` using the same method which is based on the
  number size to decide what is the appropriate handler (`one`, `dozens`,
  `hundred`, `thousands`).
- We translate the number and also handle any remaining value.
- At the end we do some verification to display `dolars` and `cents` value and
  also if the words `dolar` and `cents` should be on singular or plural form.
