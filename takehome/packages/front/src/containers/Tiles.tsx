import { useQuery } from "@apollo/client";
import React from "react"
import Flex from "../components/ui/Flex";
import TileItem from "../components/TileItem"
import { GET_TILES } from "../graphql/query/tile.query";
import { TileData } from "../types";

const Tiles: React.FunctionComponent = () => {
    const { loading, error, data } = useQuery<TileData>(GET_TILES);
    

    if(loading) {
        return <div> Loading....</div>
    }
   
    if (error) {
      return <div>Error! {error.message}</div>;
    }


    return (
    <Flex>
        {data?.getTiles.map(tile => <TileItem key={tile.id} {...tile} />)}
    </Flex>
    )


}


export default Tiles

