import React from 'react';
import { InMemoryCache, ApolloClient, NormalizedCacheObject, ApolloProvider } from '@apollo/client';
import Tiles from './Tiles';

export const cache: InMemoryCache = new InMemoryCache({});
const client: ApolloClient<NormalizedCacheObject> = new ApolloClient({
  cache,
  uri: 'http://localhost:4000/graphql',
});



const  App: React.FunctionComponent = () => {
  return (
    <ApolloProvider client={client}>
      <Tiles />
    </ApolloProvider>
  );
}

export default App;
