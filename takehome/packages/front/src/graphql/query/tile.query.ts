import { gql } from '@apollo/client';

export const GET_TILES = gql`
        query {
    getTiles {
      id
      name
      internalName
      status
      url
      tileImage
    }
  }
`;
