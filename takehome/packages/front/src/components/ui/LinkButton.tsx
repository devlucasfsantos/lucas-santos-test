import styled from "styled-components"


const LinkButton = styled.a`
    color: #fff;
    background-color: #7e2a81;
    border-radius: .9rem;
    display: flex;
    justify-content: center;
    text-decoration: none;
    font-weight: 700;
    padding: 1rem 1.5rem;
    text-transform: uppercase;
    font-family: 'Mulish', sans-serif;

`

export default LinkButton

