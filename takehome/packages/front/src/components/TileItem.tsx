import React from "react"
import { Tile } from "../types"
import styled from "styled-components";
import LinkButton from "./ui/LinkButton";
import mediaQuery from "../utils/mediaQuery";



const TileContainer = styled.div`
flex: 0 0 31%; 

${mediaQuery.tablet} {
    flex: 0 0 45%; 
  }


margin: 1%;
display: grid;
grid-template-columns: 40% 60%;
border: 1px solid #e7e7e7;
    h3 {
        color: #7e2a81f; 
        text-transform: uppercase;
        font-size: 1.5rem;
        font-family: 'Mulish', sans-serif;
    }
`

const TileDetails = styled.div`
display: flex;
flex-direction: column;
padding: 2rem 2rem 0;
`


const TileImage = styled.div`
img {
width: 300px;
height: 300px;
}
`

type TileItemProps = Partial<Tile>


const TileItem: React.FunctionComponent<TileItemProps> = (props) => {

    const { name, url, tileImage} = props;


       
        return   (
       <TileContainer>
        <TileDetails>
            <h3>{name}</h3>
    
        <LinkButton target="_blank" rel="noreferrer noopener" href={url}>View Details</LinkButton>
        </TileDetails>
        <TileImage>
            <img alt={name} src={tileImage} />
        </TileImage>
    
        </TileContainer>
        )

    

}

export default TileItem