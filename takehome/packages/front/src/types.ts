

export interface Tile {
    id: string;
    name: string;
    internalName: string;
    url: string;
    tileImage: string;
    status: boolean;
}

export interface TileData {
    getTiles: Array<Tile>
}