import { GraphQLList } from "graphql";
import { GraphqlContext } from "../../graphql";
import { APITiles } from "../../services/laybuy-client";
import { Merchant } from "./merchant";
import { tilesType } from "./merchants.types";


type Mapper<T,S> = (input: T) => S | Promise<S>

  const titlesQuery = {
      type: new GraphQLList(tilesType),
      resolve: (root: any, args: any, context: GraphqlContext, info: any ) => getTiles(context)
  }

  const getTiles = async (context: GraphqlContext) => {
    const data = await context.laybuyApiClient.getTitles();
    const response =  data.map(x => mapApiDataToGraphql(x))
    return response;
  }

  const mapApiDataToGraphql: Mapper<APITiles,Merchant> = (input:APITiles) => {
        return  new Merchant({
          id: input.id,
          name: input.attributes.name,
          tileImage: input.attributes.tileImage.url,
          url: input.attributes.url
      })

  }

  export default titlesQuery