import { GraphQLObjectType, GraphQLNonNull, GraphQLString, GraphQLBoolean } from "graphql";

export const tilesType = new GraphQLObjectType({
    name: "Tiles",
    description: "A list of merchant.",
    fields: {
      id: {
        type: GraphQLString,
        description: "The merchant id",
      },
      name: {
        type: GraphQLString,
        description: "The merchant name",
      },
      internalName: {
        type: GraphQLString,
        description: "The merchant internal name",
      },
      status: {
        type: GraphQLBoolean,
        description: "The merchant status"
      },
      url: {
        type: GraphQLString,
        description: "The merchant store url",
      },
      tileImage:  {
        type: GraphQLString,
        description: "The merchant image url.",
      }
      
    },
  });

