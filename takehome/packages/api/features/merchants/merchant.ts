


export class Merchant {

    constructor(data: Partial<Merchant>) {
        Object.assign(this,data);
    }
    
    id:string;
    name: string;
    internalName: string;
    status: string;
    url: string;
    tileImage: string;
}


