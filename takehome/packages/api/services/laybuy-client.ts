

import axios from "axios"



interface LaybuyClientConfig {
    endpoint : string 
}

const SERVICE_ENDPOINTS = {
    titles: '/tiles'
}


export interface APIResult<T> {
    data: T;
}

export interface APITiles {
    id: string;
    type: string;
    attributes: APITilesAttributes
}


export interface APITilesAttributes {
    name: string;
    internalName: string;
    url: string;
    tagMatch: boolean,
    activeStartsAt: Date;
    activeEndsAt?: Date,
    utmSource:   string;
    utmMedium:  string;
    utmCampaign:  string;
    utmTerm: string,
    utmContent: string,
    instore: true,
    online: boolean,
    global: boolean,
    comingSoon: boolean,
    tileImage: {
    url: string
    },
    logoImage: string,
    currentTileUrl: string,
    currentLogoUrl: string,
    currentUrl: string,
    status: string,
    callToAction: null,
    exactCategories: null

}



export default class LaybuyClient {

    constructor(config: LaybuyClientConfig) {
   
       axios.defaults.baseURL = config.endpoint;
    }


    public async getTitles(args: string = `page%5Bsize%5D=8&page%5Bnumber%5D=1&include=activePromotion&filter%5Border%5D=Offers%20%26%20Deals&filter%5Bcategory_id%5D=1`): Promise<Array<APITiles>> {
        const response = await axios.get<APIResult<Array<APITiles>>>(`${SERVICE_ENDPOINTS.titles}?${args}`)
        return response.data.data;

}
}
