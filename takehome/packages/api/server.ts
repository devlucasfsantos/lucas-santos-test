
import { ApolloServer } from "apollo-server-express";
import cors from "cors"
import express from 'express'
import { GraphqlContext, schema } from "./graphql";
import LaybuyClient from "./services/laybuy-client";

const DEFAULT_URL =  "https://shop-directory-heroku.laybuy.com/api"


require('dotenv').config();

const createContext: () => GraphqlContext = () => {
    return {
        laybuyApiClient: new LaybuyClient({ 
            endpoint: process.env.ENDPOINT_URL || DEFAULT_URL }
            )
    }
}


const app: express.Application = express();
const path = '/graphql';
const port = process.env.PORT || 4000

app.use(cors({
    preflightContinue: false,
    origin: "*",
    allowedHeaders: [
      "Origin",
      "X-Requested-With",
      "Content-Type",
      "Accept",
      "Authorization"
    ]
  }))

const apolloServer = new ApolloServer({
    schema,
    context: createContext(), 
    playground: true,
  });

  apolloServer.applyMiddleware({ app, path });

  app.listen(port, () => {
    console.log(`API IS RUNNING ON http://localhost:${port}${path}`);
  });