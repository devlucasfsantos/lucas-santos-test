import LaybuyClient from "./services/laybuy-client";
import  { GraphQLObjectType, GraphQLSchema} from "graphql"
import titlesQuery from "./features/merchants/merchants.query";




export type GraphqlContext = {
    laybuyApiClient: LaybuyClient
}

const query = new GraphQLObjectType({
  name: "Query",
  fields: {
    getTiles: titlesQuery
  }

});

export const schema: GraphQLSchema = new GraphQLSchema({
  query
  })

